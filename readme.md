spinraker
=========
spinnaker

```bash
vagrant up
ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook playbook.yml -i inventory -l kubernetes

wget https://github.com/rancher/rke/releases/download/v1.0.0/rke_linux-amd64
./rke_linux-amd64 up --config cluster.yml

export KUBECONFIG=$(pwd)/kube_config_cluster.yml
kubectl get pods --all-namespaces

# generate crt for nginx
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key -out crt

# helm
kubectl apply -f create-tiller-rbac-sa.yml
helm init --service-account tiller
kubectl -n kube-system get po

kubectl create ns spinnaker

# install minio
helm install --namespace spinnaker --name minio stable/minio -f minio.yml --wait

cp kube_config_cluster.yml halyard

docker-compose up -d
docker-compose exec halyard bash
cd

cd halyard
cp $(pwd)/kube_config_cluster.yml ~/.kube/config
cd

# create spinnaker service account
# https://www.spinnaker.io/setup/install/providers/kubernetes-v2/
CONTEXT=$(kubectl config current-context)
kubectl apply --context $CONTEXT -f service-account.yml

TOKEN=$(kubectl get secret --context $CONTEXT \
   $(kubectl get serviceaccount spinnaker-service-account \
       --context $CONTEXT \
       -n spinnaker \
       -o jsonpath='{.secrets[0].name}') \
   -n spinnaker \
   -o jsonpath='{.data.token}' | base64 --decode)
kubectl config set-credentials ${CONTEXT}-token-user --token $TOKEN
kubectl config set-context $CONTEXT --user ${CONTEXT}-token-user

# set kubernetes account
hal config provider kubernetes enable
hal config provider kubernetes account add dev \
    --provider-version v2 \
    --context $(kubectl config current-context)
hal config deploy edit --type distributed --account-name dev \
    --location spinnaker

# disable minio versioning
mkdir -p ~/.hal/default/profiles
cat > /home/spinnaker/.hal/default/profiles/front50-local.yml<<EOF
spinnaker.s3.versioning: false
EOF

# set minio as storage
MINIO_ENDPOINT=http://minio:9000
MINIO_SECRET_KEY=miniosecretkey
MINIO_ACCESS_KEY=minioaccesskey
echo $MINIO_SECRET_KEY | hal config storage s3 edit --endpoint $MINIO_ENDPOINT \
    --path-style-access=true \
    --access-key-id $MINIO_ACCESS_KEY \
    --secret-access-key
hal config storage edit --type s3

# fix url
hal config security api edit --override-base-url https://spinnaker-api.rke.local
hal config security ui edit --override-base-url https://spinnaker.rke.local

# ingress gate termination fix
# https://github.com/spinnaker/spinnaker/issues/1630
cat > /home/spinnaker/.hal/default/profiles/gate-local.yml<<EOF
server:
  tomcat:
    protocolHeader: X-Forwarded-Proto
    remoteIpHeader: X-Forwarded-For
    internalProxies: .*
    httpsServerPort: X-Forwarded-Port
EOF

# deploy spinnaker
hal version list
hal config version edit --version 1.17.5
hal deploy apply

# destroy
hal deploy clean
```
